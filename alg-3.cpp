#include <iostream>
#include <Windows.h>
#include <bitset>

#define COLOR_RED 64
#define COLOR_BLACK 0
#define COLOR_GREEN 32
#define COLOR_WHITE 127

using namespace std;

const int ArraySize = 20;
const int NumGenerations = 30;
const string InitialState = "01010101111100010101";

int main()
{
    bitset<ArraySize + 2> array(InitialState);

    for(int i=0; i<NumGenerations; i++)
    {
        bitset<ArraySize + 2> tmpArray(array);
        for(int j=ArraySize; j>0; j--)
        {
            if(tmpArray[j])
            {
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), COLOR_GREEN);
                cout << " ";
            }
            else
            {
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), COLOR_RED);
                cout << " ";
            }

            int value = (int)tmpArray[j-1] << 2 | (int)tmpArray[j] << 1 | (int)tmpArray[j+1];
            array[j] = (value == 3 || value == 5 || value == 6);
        }
        cout << endl;
        cout << endl;
    }
    return 0;
}
