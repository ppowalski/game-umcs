const game = document.getElementById("game");

const height = game.height = 600;
const width = game.width = 600;

const context = game.getContext("2d");

let isGame = true;
let drawLine = false;

context.fillStyle = "#FFF";
context.font = "100px Georgia";


//Aktualna tura
let tura = "O";

//Tablica wartosci
let tab = [[],[],[]];


//pozycje figur
const posX = [25, 225, 425];
const posY = [170, 365, 570];

const posP = [0, 195, 395];

//Pionowe kreski
context.fillRect(posP[1],0, 5, height); 
context.fillRect(posP[2],0, 5, height); 

//Poziome kreski
context.fillRect(0, posP[1], width, 5); 
context.fillRect(0, posP[2], width, 5); 

function refresh()
{
	for(let i=0; i<3; i++)
	{
		for(let j=0; j<3; j++)
		{
			if(tab[i][j] != undefined)
			{
				context.font = "200px Georgia";
				context.fillStyle = "#FFF";

				context.fillText(tab[i][j], posX[i], posY[j]);
			}
		}
	}
}

let interval;
function position(x, y)
{
	if(!isGame) return 0;

	let start_x = (window.innerWidth-width)/2;
	let end_x = start_x+width;
	let start_y = (window.innerHeight-height)/2;
	let end_y = start_y+height;

	for(let i=0; i<3; i++)
	{
		for(let j=0; j<3; j++)
		{
			if((posP[i]+start_x < x && posP[i]+start_x+200 > x) && (posP[j]+start_y < y && posP[j]+start_y+200 > y))
			{
				if(tab[i][j] == undefined)
				{
					tab[i][j] = tura;

					let result = evaluate(tab);
					
					if(result == 0 && !jestRuch(tab))
					{
						refresh();
						remis();
						isGame = false;
						return 0;
					}

					if(result == 1 || result == -1)
					{
						interval = window.setInterval(win_k1, 1000/60);
						isGame = false;
					}
					else if(result == 2 || result == -2)
					{
						interval = window.setInterval(win_k2, 1000/60);
						isGame = false;
					}
					else if(result == 3 || result == -3)
					{
						interval = window.setInterval(win_k3, 1000/60);
						isGame = false;
					}
					else if(result == 4 || result == -4)
					{
						interval = window.setInterval(win_r1, 1000/60);
						isGame = false;
					}
					else if(result == 5 || result == -5)
					{
						interval = window.setInterval(win_r2, 1000/60);
						isGame = false;
					}
					else if(result == 6 || result == -6)
					{
						interval = window.setInterval(win_r3, 1000/60);
						isGame = false;
					}
					else if(result == -10 || result == 10)
					{
						interval = window.setInterval(win_sr, 1000/60);
						isGame = false;
					}
					else if(result == -11 || result == 11)
					{
						interval = window.setInterval(win_sl, 1000/60);
						isGame = false;
					}
					refresh();
					if(isGame)
					{
						findBestMove(tab);
						refresh();
					}
				}
			}
		}
	}
}


let iter=1;
function win_sr()
{
	if(iter >= 900) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);

		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}

	context.fillStyle = "#8e020e";
	context.beginPath();
	context.resetTransform();
	context.translate(0, 0);
	context.rotate(135*Math.PI/180);
	context.rect(-2.5, -iter, 5, iter);
	context.fill();
	context.restore();
	iter+=10;
}

function win_sl()
{
	if(iter >= 900) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);
		
		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}
	
	context.fillStyle = "#8e020e";
	context.beginPath();
	context.resetTransform();
	context.translate(600, 0);
	context.rotate(-135*Math.PI/180);
	context.rect(0, -iter, 5, iter);
	context.fill();
	context.restore();
	iter+=10;
}

function win_k1()
{
	if(iter >= 600) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);

		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}

	context.fillStyle = "#8e020e";
	context.fillRect(posP[0]+95,0, 5, iter);
	iter+=10;
}

function win_k2()
{
	if(iter >= 600) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);

		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}

	context.fillStyle = "#8e020e";
	context.fillRect(posP[1]+100,0, 5, iter);
	iter+=10;
}

function win_k3()
{
	if(iter >= 600) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);

		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}

	context.fillStyle = "#8e020e";
	context.fillRect(posP[2]+100,0, 5, iter);
	iter+=10;
}

function win_r1()
{
	if(iter >= 600) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);

		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}

	context.fillStyle = "#8e020e";
	context.fillRect(0, posP[0]+95, iter, 5); 
	iter+=10;
}

function win_r2()
{
	if(iter >= 600) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);

		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}

	context.fillStyle = "#8e020e";
	context.fillRect(0, posP[1]+100, iter, 5); 
	iter+=10;
}

function win_r3()
{
	if(iter >= 600) 
	{
		window.clearInterval(interval);
		let result = evaluate(tab);

		if(result > 0)
		{
			youLose();
			return 0;
		}
		else if(result < 0)
		{
			youWin();
			return 0;
		}
	}

	context.fillStyle = "#8e020e";
	context.fillRect(0, posP[2]+100, iter, 5); 
	iter+=10;
}

/*
function miniMax(depth, nodeIndex, isMax, scores, alpha, beta)
{
	let tab = scores;
	
	if(depth == 3)
	{
		return evaluate(tab);
	}
	
	let best = isMax ? -1000 : 1000;
	
	for(let i=0; i<2; i++)
	{
		let value = miniMax(depth+1, nodeIndex*2+i, !isMax, scores, alpha, beta);
		best = isMax ? Math.max(best, value) : Math.min(best, value);
		
		if(isMax)
		{
			alpha = Math.max(alpha, best);
		}
		else
		{
			beta = Math.min(beta, best);
		}
		
		if(beta <= alpha) break;
	}
	
	return best;
}*/

const player = "O";
const opponent = "X";

function minimax(board, depth, isMax) 
{ 
    let score = evaluate(board); 
  
    // If Maximizer has won the game return his/her 
    // evaluated score 
    if (isMax && score > 0) 
        return score; 
	
	if(!isMax && score < 0)
	{
		return score;
	}
  
    // If this maximizer's move 
    if (isMax) 
    { 
        let best = -1000; 
  
        // Traverse all cells 
        for (let i = 0; i<3; i++) 
        { 
            for (let j = 0; j<3; j++) 
            { 
                // Check if cell is empty 
                if (board[i][j]==undefined) 
                { 
                    // Make the move 
                    board[i][j] = player; 
  
                    // Call minimax recursively and choose 
                    // the maximum value 
                    best = Math.max( best, 
                        minimax(board, depth+1, !isMax) ); 
  
                    // Undo the move 
                    board[i][j] = undefined; 
                } 
            } 
        } 
        return best; 
    } 
  
    // If this minimizer's move 
    else
    { 
        let best = 1000; 
  
        // Traverse all cells 
        for (let i = 0; i<3; i++) 
        { 
            for (let j = 0; j<3; j++) 
            { 
                // Check if cell is empty 
                if (board[i][j]==undefined) 
                { 
                    // Make the move 
                    board[i][j] = opponent; 
  
                    // Call minimax recursively and choose 
                    // the minimum value 
                    best = Math.min(best, 
                           minimax(board, depth+1, !isMax)); 
  
                    // Undo the move 
                    board[i][j] = undefined; 
                } 
            } 
        } 
        return best; 
    } 
} 
  
// This will return the best possible move for the player 
function findBestMove(board) 
{ 
    let bestVal = -1000; 
	
    let bestMoveRow = -1; 
    let bestMoveCol = -1; 
  
    // Traverse all cells, evaluate minimax function for 
    // all empty cells. And return the cell with optimal 
    // value. 
    for (let i = 0; i<3; i++) 
    { 
        for (let j = 0; j<3; j++) 
        { 
            // Check if cell is empty 
            if (board[i][j]==undefined) 
            { 
                // Make the move 
                board[i][j] = player; 
  
                // compute evaluation function for this 
                // move. 
                let moveVal = minimax(board, 0, false); 
  
                // Undo the move 
                board[i][j] = undefined; 
  
                // If the value of the current move is 
                // more than the best value, then update 
                // best/ 
                if (moveVal > bestVal) 
                { 
                    bestMoveRow = i; 
                    bestMoveCol = j; 
                    bestVal = moveVal; 
                } 
            } 
        } 
    } 
	
    board[bestMoveRow][bestMoveCol] = opponent;	
} 

function evaluate(tab)
{
	for(let i=0; i<3; i++)
	{
		if(tab[i][0] == tab[i][1] && tab[i][1] == tab[i][2])
		{
			if(tab[i][0] == 'X')
			{
				return 1+i;
			}
			else if(tab[i][0] == 'O')
			{
				return -1-i;
			}
		}
	}
	
	for(let i=0; i<3; i++)
	{
		if(tab[0][i] == tab[1][i] && tab[1][i] == tab[2][i])
		{
			if(tab[0][i] == 'X')
			{
				return 1+i+3;
			}
			else if(tab[0][i] == 'O')
			{
				return -1-i-3;
			}
		}
	}
	
	if(tab[0][0] == tab[1][1] && tab[1][1] == tab[2][2])
	{
		if(tab[0][0] == 'X')
		{
			return 10;
		}
		else if(tab[0][0] == 'O')
		{
			return -10;
		}
	}
	
	if(tab[2][0] == tab[1][1] && tab[2][0] == tab[0][2])
	{
		if(tab[2][0] == 'X')
		{
			return 11;
		}
		else if(tab[2][0] == 'O')
		{
			return -11;
		}
	}
	
	return 0;
}

function jestRuch(tab) 
{ 
    for(let i = 0; i<3; i++) 
	{
        for(let j = 0; j<3; j++) 
		{
            if(tab[i][j]==undefined) 
			{
                return true; 
			}
		}
	}
    return false; 
} 

function youWin()
{
	context.resetTransform();
	context.font = "60px Georgia";
	context.fillStyle = "green";
	context.fillRect((600-300)/2, (600-150)/2, 300, 150);
	context.fillStyle = "#FFF";
	context.fillText("Wygrales!", (600-260)/2, (600+40)/2);
}

function youLose()
{
	context.resetTransform();
	context.font = "60px Georgia";
	context.fillStyle = "red";
	context.fillRect((600-300)/2, (600-150)/2, 300, 150);
	context.fillStyle = "#FFF";
	context.fillText("Przegrales!", (600-290)/2, (600+40)/2);
}

function remis()
{
	context.font = "60px Georgia";
	context.fillStyle = "#ff9900";
	context.fillRect((600-300)/2, (600-150)/2, 300, 150);
	context.fillStyle = "#FFF";
	context.fillText("Remis!", (600-190)/2, (600+40)/2);
}

addEventListener("click", function(e) {
	position(e.clientX, e.clientY);
});